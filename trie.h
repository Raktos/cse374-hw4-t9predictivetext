// trie.h
// Jason Ho
// 11 February 2016
// header file for trie.c
// defines public functions from trie.c

#ifndef TRIE_H
#define TRIE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

// Node struct
typedef struct node {
  char *data;
  struct node **children;
} Node;

// create a trie from a file given as input
Node* create_trie(FILE *f);

// takes a trie Node* and a string in t9 format
// finds if the string of t9 input exists in the trie
// returns the result as a string if it does, NULL if it does not
char* lookup(Node *trie, char *str);

// destroyes a trie recursively
void destroy_trie(Node* trie);

#endif
