# makefile
# Jason Ho
# 11 February 2016
# makefile for t9

t9 : trie.o main.o
	gcc -Wall -std=c11 -o t9 trie.o main.o

trie.o : trie.h trie.c
	gcc -Wall -std=c11 -g -c trie.c trie.h

main.o : main.c trie.h
	gcc -Wall -std=c11 -g -c main.c trie.h

# # is the comment character for makefiles
# We can have 'phony' rules with no prerequisites to perform special-case jobs.
# Since there are no prerequisites, make will always run the commands for the
# rule.
.PHONY : clean # we need to do this to prevent make from getting confused if
               # there is an acutal file named clean
# a clean rule is a useful to 'reset' the codebase to just the source files
# run with the command 'make clean'
clean :
	rm -f trie.o main.o t9
