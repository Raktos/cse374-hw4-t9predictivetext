// main.c
// Jason Ho
// 11 February 2016
// Manages an interactive user session with a t9 trie
// exits with 1 if invalid arguments
// exits with 2 if it cannot read a file
// exits with 3 if memory cannot be allocated

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "trie.h"

#define MAX_INPUT_LENGTH 100

int main(int argc, char **argv) {
  // only allow the correct amount of arguments
  if (argc != 2) {
    fprintf(stderr, "usage: t9 [FILE]");
    exit(1);
  } else {
    // open the file to read from
    FILE *f = fopen(argv[1], "r");
    if (f == NULL) {
      fprintf(stderr, "Cannot find file %s\n", argv[1]);
      exit(2);
    }

    // create the trie from the file
    Node *trie = create_trie(f);

    // input is the raw input
    // lookupStr is the actual string used to lookup
    char input[MAX_INPUT_LENGTH];
    char lookupStr[MAX_INPUT_LENGTH * 2];

    printf("Enter \"exit\" to quit.\n");
    printf("Enter key sequence (or \"#\" for next word)\n");
    while (fgets(input, MAX_INPUT_LENGTH, stdin)) {
      // get rid of pesky \n
      int inputLen = strlen(input);
      if (inputLen > 0 && input[inputLen - 1] == '\n') {
        input[inputLen - 1] = '\0';
        inputLen--;
      }

      // special cases for input
      if (strncmp(input, "exit", 4) == 0) {
        break;
      } else if (strlen(input) <= 0) {
        // do nothing if the string is empty, just restart the loop
      } else if (input[0] == '#') {
        // concatanate to the last lookup, we're in the same sequence
        strncat(lookupStr, input, strlen(input));
      } else {
        // straight copy to lookupStr, we have a new sequence
        strncpy(lookupStr, input, MAX_INPUT_LENGTH * 2);
      }

      // do the actual lookup
      char *result = lookup(trie, lookupStr);

      // print results
      if (result == NULL) {
        if (lookupStr[strlen(lookupStr) - 1] == '#') {
          printf("no more t9onyms\n");
        } else {
          printf("Not in current dictionary\n");
        }
      } else {
        printf("%s\n", result);
      }

      printf("Enter key sequence (or \"#\" for next word)\n");
    }

    destroy_trie(trie);
  }

  return 0;
}
