// trie.c
// Jason Ho
// 11 February 2016
// creates a trie based on t9 input
// exits with 2 when it cannot read a file
// exits with 3 when memory cannot be allocated

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "trie.h"

#define MAX_READ_WORD_LENGTH 100

Node* init_node();
Node* insert(Node *trie, char *str);
int char_to_keypress(char c);
int keypress_to_index(int i);
int char_to_index(char c);
int input_to_index(char c);
void destroy_node(Node *n);

// creates a trie from a file given as input
Node* create_trie(FILE *f) {
  Node *trie = init_node();

  if (f != NULL) {
    char line[MAX_READ_WORD_LENGTH];

    while (fgets(line, MAX_READ_WORD_LENGTH, f)) {
      insert(trie, line);
    }
  } else {
    fprintf(stderr, "trie.c: cannot read file");
    exit(2);
  }

  return trie;
}

// takes a trie Node* and a string in t9 format
// finds if the string of t9 input exists in the trie
// returns the result as a string if it does, NULL if it does not
char* lookup(Node *trie, char *str) {
  // go down the trie to the correct node (excluding hash travels)
  for (int i = 0; i < strlen(str); i++) {
    int index = input_to_index(str[i]);

    if (trie->children[index] == NULL) {
      return NULL;
    } else {
      trie = trie->children[index];
    }
  }

  return trie->data;
}

// destroys a trie recursively
void destroy_trie(Node* trie) {
  // if there are children, recursively destroy them
  for (int i = 0; i < 9; i++) {
    if (trie->children[i] != NULL) {
      destroy_trie(trie->children[i]);
    }
  }

  // destroy the array containing pointers to children
  if (trie->children != NULL) {
    free(trie->children);
  }

  if (trie->data != NULL) {
    free(trie->data);
  }

  free(trie);

  return;
}

// initializes a blank node
// intiaizes the array of pointers to the children nodes
// sets the data to NULL.
// Returns the created node
Node* init_node() {
  Node *n = malloc(sizeof(*n));
  if (n == NULL) {
    fprintf(stderr, "Failed to allocate memory in init_node()");
    exit(3);
  }

  n->data = NULL;
  n->children = calloc(9, sizeof(n->children));

  if (n->children == NULL) {
    fprintf(stderr, "Failed to allocate memory to children in init_node()");
    exit(3);
  }

  return n;
}

// inserts a word into a trie
// takes a Node* to insert into and a string
Node* insert(Node *trie, char *str) {
  int len = strlen(str);

  // get rid of any pesky endlines
  if (str[len - 1] == '\n') {
    str[len - 1] = '\0';
    len--;
  }

  // go down the trie to the right place
  for (int i = 0; i < len; i++) {
    int index = char_to_index(str[i]);

    if (trie->children[index] == NULL) {
      trie->children[index] = init_node();
    }

    trie = trie->children[index];
  }

  // move through the #'s
  while (trie->data != NULL) {
    if (trie->children[8] == NULL) {
      trie->children[8] = init_node();
    }

    trie = trie->children[8];
  }

  // get memory for the data string and copy the passed str into it
  trie->data = malloc(strlen(str) + 1);
  strncpy(trie->data, str, strlen(str) + 1);

  return trie;
}

// takes a character and returns the associated keypress.
// returns -1 on an illegal character
int char_to_keypress(char c) {
  c = tolower(c);
  if (c  >= 'a' && c <= 'c') {
    return 2;
  } else if (c >= 'd' && c <= 'f') {
    return 3;
  } else if (c >= 'g' && c <= 'i') {
    return 4;
  } else if (c >= 'j' && c <= 'l') {
    return 5;
  } else if (c >= 'm' && c <= 'o') {
    return 6;
  } else if (c >= 'p' && c <= 's') {
    return 7;
  } else if (c >= 't' && c <= 'v') {
    return 8;
  } else if (c >= 'w' && c <= 'z') {
    return 9;
  } else if (c == '#') {
    return 10;
  }

  return -1;
}

// returns the expected array index of a keypress
// returns -1 on an illegal key
int keypress_to_index(int i) {
  if (i >= 2 && i <= 10) {
    return i - 2;
  }
  return -1;
}

// returns the expected array index of a character
// returns -1 on an illegal character
int char_to_index(char c) {
  return keypress_to_index(char_to_keypress(c));
}

// takes t9 input and returns the corresponding array index
// will fail horribly and silently if it recieves an invalid t9 character
int input_to_index(char c) {
  if (c == '#') {
    return 8;
  } else {
    return (c - '0') - 2;
  }
}
